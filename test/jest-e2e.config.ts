import { JestConfigWithTsJest } from 'ts-jest';

const jestE2eConfig = {
    moduleFileExtensions: ['js', 'json', 'ts'],
    rootDir: '.',
    testEnvironment: 'node',
    testRegex: '.e2e-spec.ts$',
    transform: {
        '^.+\\.(t|j)s$': 'ts-jest',
    },
} satisfies JestConfigWithTsJest;

export default jestE2eConfig;
