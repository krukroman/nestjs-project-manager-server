import { NoErrorThrownError } from './no-error-thrown-error';

export function getErrorSync(cb: () => unknown): unknown {
    try {
        cb();

        throw new NoErrorThrownError();
    } catch (error) {
        return error;
    }
}

export async function getErrorAsync(
    cb: () => Promise<unknown>,
): Promise<unknown> {
    try {
        await cb();

        throw new NoErrorThrownError();
    } catch (error) {
        return error;
    }
}
