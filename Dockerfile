FROM node:20.15.0-alpine3.20 AS build

WORKDIR /home/app

COPY package*.json ./

RUN --mount=type=cache,target=/home/app/.npm \
    npm set cache /home/app/.npm && \
    npm ci

COPY nest-cli.json ./
COPY tsconfig*.json ./
COPY src ./src
COPY drizzle ./drizzle
COPY --chmod=755 start.sh ./

RUN npm run build

FROM node:20.15.0-alpine3.20 AS prod

WORKDIR /home/server

ENV NODE_ENV="production"

COPY --from=build /home/app/package*.json .

RUN --mount=type=cache,target=/home/server/.npm \
    npm set cache /home/server/.npm && \
    npm ci --only=production

USER node

COPY --from=build --chown=node:node /home/app/dist ./dist
COPY --from=build --chown=node:node /home/app/drizzle ./drizzle
COPY --from=build --chown=node:node /home/app/start.sh ./

EXPOSE ${PORT}

SHELL [ "/bin/sh", "-c" ]

ENTRYPOINT [ "/home/server/start.sh" ]