import process from 'node:process';
import { defineConfig } from 'drizzle-kit';

const drizzleConfig = defineConfig({
    dialect: 'postgresql',
    out: './drizzle',
    schema: './src/database/database.schema.ts',
    dbCredentials: {
        url: process.env.DATABASE_URL!,
    },
});

export default drizzleConfig;
