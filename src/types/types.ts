import { SORT_ORDER, ENV_MODES } from 'src/constants';

export type ENV_VARS = {
    NODE_ENV: (typeof ENV_MODES)[number];
    PORT: number;
    DATABASE_URL: string;
};

export type PageInfo = {
    page: number;
    pageSize: number;
};

export type SortOrder = (typeof SORT_ORDER)[number];

export type EntityDtoOut<T> = {
    data: T;
};
