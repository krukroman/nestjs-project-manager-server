export const DB_CONNECTION_TOKEN = 'DB_CONNECTION';

export const PROD = 'production';
export const DEV = 'development';

export const ASC = 'asc';
export const DESC = 'desc';

export const SORT_ORDER = Object.freeze([ASC, DESC] as const);

export const ENV_MODES = Object.freeze([PROD, DEV] as const);
