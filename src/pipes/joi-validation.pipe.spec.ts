import { Schema, ValidationError } from 'joi';

import { getErrorAsync, NoErrorThrownError } from 'test/test-utils';

import { JoiValidationPipe } from './joi-validation.pipe';
import {
    InternalServerErrorException,
    UnprocessableEntityException,
} from '@nestjs/common';

describe('JoiValidationPipe', function () {
    it('should be defined.', function () {
        expect(JoiValidationPipe).toBeDefined();
    });
});

describe('transform', function () {
    const schema = {
        validateAsync: jest.fn(),
    };
    let pipe: JoiValidationPipe<unknown>;

    beforeEach(function () {
        pipe = new JoiValidationPipe<unknown>(schema as unknown as Schema);
    });

    afterEach(function () {
        schema.validateAsync.mockClear();
    });

    afterAll(function () {
        schema.validateAsync.mockReset();
    });

    it('should return a result from schema validation function.', async function () {
        const mockedResult = {
            key: 'value',
        };
        schema.validateAsync.mockResolvedValue(mockedResult);

        const result = await pipe.transform(mockedResult, {} as any);

        expect(result).toStrictEqual(mockedResult);
        expect(schema.validateAsync).toHaveBeenCalledTimes(1);
    });

    it('should throw an Unprocessable entity exception if validation failed .', async function () {
        const mockedError = new ValidationError('Validation failed', [], null);
        schema.validateAsync.mockRejectedValue(mockedError);

        const testFn = async function () {
            await pipe.transform({}, {} as any);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(UnprocessableEntityException);
        expect(schema.validateAsync).toHaveBeenCalledTimes(1);
    });

    it('should throw an Internal server exception if something went wrong with schema.', async function () {
        const mockedError = new Error('some error');
        schema.validateAsync.mockRejectedValue(mockedError);

        const testFn = async function () {
            await pipe.transform({}, {} as any);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(schema.validateAsync).toHaveBeenCalledTimes(1);
    });
});
