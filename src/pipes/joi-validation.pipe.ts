import {
    ArgumentMetadata,
    Injectable,
    InternalServerErrorException,
    PipeTransform,
    UnprocessableEntityException,
} from '@nestjs/common';

import Joi from 'joi';

@Injectable()
export class JoiValidationPipe<R>
    implements PipeTransform<unknown, Promise<R>>
{
    constructor(private readonly joiSchema: Joi.Schema<R>) {}

    async transform(value: unknown, _: ArgumentMetadata): Promise<R> {
        try {
            const result = await this.joiSchema.validateAsync(value, {
                abortEarly: false,
            });

            return result;
        } catch (error) {
            if (Joi.isError(error)) {
                throw new UnprocessableEntityException({
                    message: 'Validation failed.',
                    cause: error.details,
                });
            }

            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }
}
