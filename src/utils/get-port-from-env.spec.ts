import process from 'node:process';

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { getErrorSync, NoErrorThrownError } from 'test/test-utils';

import { getPortFromEnv } from './get-port-from-env';

describe('getPortFromEnv', function () {
    const originalEnv = process.env;
    let configService: ConfigService;

    beforeEach(async function () {
        process.env = { ...originalEnv };
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    ignoreEnvFile: true,
                }),
            ],
        }).compile();

        configService = module.get<ConfigService>(ConfigService);
    });

    afterEach(function () {
        process.env = originalEnv;
    });

    it('should return a defined value from env.', function () {
        const key = 'PORT';
        const value = '4321';
        const expectedValue = parseInt(value, 10);

        process.env[key] = value;

        const result = getPortFromEnv(configService);

        expect(result).toStrictEqual(expectedValue);
    });

    it('should throw an error if value is undefined.', function () {
        const testFn = function () {
            getPortFromEnv(configService);
        };

        const error = getErrorSync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toHaveProperty('message', 'Value is undefined.');
    });

    it('should throw an error if value is not a number.', function () {
        const key = 'PORT';
        const value = 'abc';

        process.env[key] = value;

        const testFn = function () {
            getPortFromEnv(configService);
        };

        const error = getErrorSync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toHaveProperty('message', 'Value is not a number.');
    });
});
