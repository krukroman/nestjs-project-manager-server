import { ConfigService } from '@nestjs/config';

export function getValueFromEnv(
    key: string,
    configService: ConfigService,
): string | never {
    const value = configService.get<string>(key);

    if (!value) {
        throw new Error('Value is undefined.', {
            cause: {
                key,
            },
        });
    }

    return value;
}
