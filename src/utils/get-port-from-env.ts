import { ConfigService } from '@nestjs/config';

import { getValueFromEnv } from './get-value-from-env';

export function getPortFromEnv(configService: ConfigService): number | never {
    const key = 'PORT';
    const portString = getValueFromEnv(key, configService);
    const portNumber = parseInt(portString, 10);

    if (isNaN(portNumber)) {
        throw new Error('Value is not a number.', {
            cause: {
                key,
            },
        });
    }

    return portNumber;
}
