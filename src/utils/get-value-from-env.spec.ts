import process from 'node:process';

import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { getErrorSync, NoErrorThrownError } from 'test/test-utils';

import { getValueFromEnv } from './get-value-from-env';

describe('getValueFromEnv', function () {
    const originalEnv = process.env;
    let configService: ConfigService;

    beforeEach(async function () {
        process.env = { ...originalEnv };
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    ignoreEnvFile: true,
                }),
            ],
        }).compile();

        configService = module.get<ConfigService>(ConfigService);
    });

    afterEach(function () {
        process.env = originalEnv;
    });

    it('should return a defined value from env.', function () {
        const key = 'ABC';
        const value = 'some value';

        process.env[key] = value;

        const result = getValueFromEnv(key, configService);

        expect(result).toStrictEqual(value);
    });

    it('should throw an error if value is undefined.', function () {
        const key = 'ABC';

        const testFn = function () {
            getValueFromEnv(key, configService);
        };

        const error = getErrorSync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toHaveProperty('message', 'Value is undefined.');
    });
});
