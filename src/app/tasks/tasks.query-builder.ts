import { Injectable } from '@nestjs/common';
import { and, eq, ilike, inArray, or, SQLWrapper } from 'drizzle-orm';

import * as schema from 'src/database/database.schema';

import { TASKS_ACTIVE_STATES } from './tasks.constants';
import { Task } from './tasks.model';

@Injectable()
export class TasksQueryBuilder {
    buildFindManyQuery(dtoIn: Partial<Task>): (SQLWrapper | undefined)[] {
        const orValues: (SQLWrapper | undefined)[] = [];
        const andValues: (SQLWrapper | undefined)[] = [];
        const whereValues: (SQLWrapper | undefined)[] = [];

        if (dtoIn.title) {
            const orValue = ilike(schema.tasks.title, `%${dtoIn.title}%`);
            orValues.push(orValue);
        }

        if (dtoIn.description) {
            const orValue = ilike(
                schema.tasks.description,
                `%${dtoIn.description}%`,
            );
            orValues.push(orValue);
        }

        if (dtoIn.id) {
            const andValue = eq(schema.tasks.id, dtoIn.id);
            andValues.push(andValue);
        }

        if (dtoIn.state) {
            const andValue = eq(schema.tasks.state, dtoIn.state);
            andValues.push(andValue);
        } else {
            const andValue = inArray(
                schema.tasks.state,
                TASKS_ACTIVE_STATES as any,
            );
            andValues.push(andValue);
        }

        if (dtoIn.priority) {
            const andValue = eq(schema.tasks.priority, dtoIn.priority);
            andValues.push(andValue);
        }

        if (dtoIn.project_id) {
            const andValue = eq(schema.tasks.project_id, dtoIn.project_id);
            andValues.push(andValue);
        }

        if (orValues.length > 0) {
            whereValues.push(or(...orValues));
        }

        if (andValues.length > 0) {
            whereValues.push(and(...andValues));
        }

        return whereValues;
    }

    buildFindOneQuery(dtoIn: Partial<Task>): (SQLWrapper | undefined)[] {
        const dtoInKeys = Object.keys(dtoIn) as (keyof Partial<Task>)[];
        const whereValues: (SQLWrapper | undefined)[] = [];

        dtoInKeys.forEach(function (key) {
            const column = schema.tasks[key];
            const value = dtoIn[key] as Task[keyof Task];
            const whereValue = eq(column, value);
            whereValues.push(whereValue);
        });

        return whereValues;
    }

    buildUpdateOneQuery(dtoIn: Partial<Task>): SQLWrapper[] {
        const dtoInKeys = Object.keys(dtoIn) as (keyof Partial<Task>)[];
        const whereValues: SQLWrapper[] = [];

        if (dtoInKeys.length === 0) {
            throw new Error('dtoIn should have at least one key-value pair.', {
                cause: dtoIn,
            });
        }

        dtoInKeys.forEach(function (key) {
            const column = schema.tasks[key];
            const value = dtoIn[key] as Task[keyof Task];
            const whereValue = eq(column, value);
            whereValues.push(whereValue);
        });

        return whereValues;
    }

    buildDeleteOneQuery(dtoIn: Partial<Task>): SQLWrapper[] {
        const dtoInKeys = Object.keys(dtoIn) as (keyof Partial<Task>)[];
        const whereValues: SQLWrapper[] = [];

        if (dtoInKeys.length === 0) {
            throw new Error('dtoIn should have at least one key-value pair.', {
                cause: dtoIn,
            });
        }

        dtoInKeys.forEach(function (key) {
            const column = schema.tasks[key];
            const value = dtoIn[key] as Task[keyof Task];
            const whereValue = eq(column, value);
            whereValues.push(whereValue);
        });

        return whereValues;
    }
}
