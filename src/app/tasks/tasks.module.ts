import { Module } from '@nestjs/common';

import { ProjectsQueryBuilder } from 'src/app/projects/projects.query-builder';
import { ProjectsRepository } from 'src/app/projects/projects.repository';
import { ProjectsService } from 'src/app/projects/projects.service';

import { TasksController } from './tasks.controller';
import { TasksQueryBuilder } from './tasks.query-builder';
import { TasksRepository } from './tasks.repository';
import { TasksService } from './tasks.service';

@Module({
    providers: [
        TasksService,
        TasksRepository,
        TasksQueryBuilder,
        ProjectsService,
        ProjectsQueryBuilder,
        ProjectsRepository,
    ],
    controllers: [TasksController],
})
export class TasksModule {}
