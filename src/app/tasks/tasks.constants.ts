import { DESC } from 'src/constants';

export const TASK_CREATED_STATE = 'created';
export const TASK_IN_PROGRESS_STATE = 'in_progress';
export const TASK_COMPLETED_STATE = 'completed';
export const TASK_ARCHIVED_STATE = 'archived';

export const TASKS_STATES = Object.freeze([
    TASK_CREATED_STATE,
    TASK_IN_PROGRESS_STATE,
    TASK_COMPLETED_STATE,
    TASK_ARCHIVED_STATE,
] as const);

export const TASKS_ACTIVE_STATES = Object.freeze([
    TASK_CREATED_STATE,
    TASK_IN_PROGRESS_STATE,
] as const);

export const TASKS_INACTIVE_STATES = Object.freeze([
    TASK_COMPLETED_STATE,
    TASK_ARCHIVED_STATE,
] as const);

export const TASKS_UPDATABLE_STATES = Object.freeze([
    TASK_CREATED_STATE,
    TASK_IN_PROGRESS_STATE,
    TASK_COMPLETED_STATE,
    TASK_ARCHIVED_STATE,
] as const);

export const TASKS_SORTABLE_FIELDS = Object.freeze([
    'title',
    'state',
    'priority',
    'created_at',
] as const);

export const TASKS_DEFAULT_SORTABLE_FIELD = 'created_at';

export const TASKS_DEFAULT_SORT_ORDER = DESC;

export const TASKS_PAGE_SIZE_LIMIT = 300;

export const TASKS_DEFAULT_PAGE_INFO = Object.freeze({
    PAGE: 1,
    PAGE_SIZE: 100,
});

export const TASKS_PRIORITIES = Object.freeze([
    'A', // must do
    'B', // should do,
    'C', // could do,
    'D', // delegate,
    'E', // eliminate
] as const);
