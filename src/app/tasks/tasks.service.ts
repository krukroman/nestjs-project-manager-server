import {
    BadRequestException,
    Injectable,
    InternalServerErrorException,
    NotFoundException,
} from '@nestjs/common';

import {
    PROJECT_ACTIVE_STATE,
    PROJECT_CREATED_STATE,
    PROJECT_DRAFT_STATE,
    PROJECTS_ACTIVE_STATES,
    PROJECTS_INACTIVE_STATES,
} from 'src/app/projects/projects.constants';
import { UpdateProjectDtoIn } from 'src/app/projects/projects.model';
import { ProjectsService } from 'src/app/projects/projects.service';

import {
    TASK_CREATED_STATE,
    TASKS_DEFAULT_PAGE_INFO,
    TASKS_DEFAULT_SORT_ORDER,
    TASKS_DEFAULT_SORTABLE_FIELD,
    TASKS_PAGE_SIZE_LIMIT,
} from './tasks.constants';
import {
    CreateTaskDtoIn,
    DeleteTaskDtoIn,
    FindManyTaskDtoIn,
    FindOneTaskDtoIn,
    Task,
    TaskDtoOut,
    TaskInsert,
    TasksDtoOut,
    TaskSortObject,
    UpdateTaskDtoIn,
} from './tasks.model';
import { TasksQueryBuilder } from './tasks.query-builder';
import { TasksRepository } from './tasks.repository';

@Injectable()
export class TasksService {
    constructor(
        private readonly queryBuilder: TasksQueryBuilder,
        private readonly tasksRepository: TasksRepository,
        private readonly projectService: ProjectsService,
    ) {}

    async findMany(dtoIn: FindManyTaskDtoIn): Promise<TasksDtoOut> {
        const sortObject = {
            sortBy: dtoIn.sortBy || TASKS_DEFAULT_SORTABLE_FIELD,
            sortOrder: dtoIn.sortOrder || TASKS_DEFAULT_SORT_ORDER,
        } satisfies TaskSortObject;

        const page = dtoIn.page || TASKS_DEFAULT_PAGE_INFO.PAGE;
        let pageSize = dtoIn.pageSize || TASKS_DEFAULT_PAGE_INFO.PAGE_SIZE;

        if (pageSize > TASKS_PAGE_SIZE_LIMIT) {
            pageSize = TASKS_PAGE_SIZE_LIMIT;
        }

        const pageInfo = {
            page,
            pageSize,
        };

        const tasksDtoIn: Partial<Task> = {};

        if (dtoIn.search) {
            tasksDtoIn.title = dtoIn.search;
            tasksDtoIn.description = dtoIn.search;
        }

        if (dtoIn.state) {
            tasksDtoIn.state = dtoIn.state;
        }

        if (dtoIn.priority) {
            tasksDtoIn.priority = dtoIn.priority;
        }

        if (dtoIn.project_id) {
            tasksDtoIn.project_id = dtoIn.project_id;
        }

        const filter = this.queryBuilder.buildFindManyQuery(tasksDtoIn);

        const tasks = await this.tasksRepository.findMany(
            filter,
            sortObject,
            pageInfo,
        );

        const dtoOut = {
            data: tasks,
        };

        return dtoOut;
    }

    async findOneById(dtoIn: FindOneTaskDtoIn): Promise<TaskDtoOut> {
        const filter = this.queryBuilder.buildFindOneQuery(dtoIn);

        const tasks = await this.tasksRepository.findOne(filter);

        const task = tasks[0];

        if (!task?.id) {
            throw new NotFoundException({
                message: 'Task not found.',
                cause: {
                    id: dtoIn.id,
                },
            });
        }

        const dtoOut = {
            data: task,
        };

        return dtoOut;
    }

    async createOne(dtoIn: CreateTaskDtoIn): Promise<TaskDtoOut> {
        const { data: project } = await this.projectService.findOneById({
            id: dtoIn.project_id,
        });

        const isDraftProject = project.state === PROJECT_DRAFT_STATE;
        const isInActiveProject = PROJECTS_INACTIVE_STATES.includes(
            project.state as any,
        );

        if (isDraftProject || isInActiveProject) {
            throw new BadRequestException({
                message:
                    'Task creation for inactive or draft project is forbidden.',
                cause: {
                    project_id: project.id,
                    project_state: project.state,
                    allowed_states: PROJECTS_ACTIVE_STATES,
                },
            });
        }

        const taskDtoIn: TaskInsert = {
            ...dtoIn,
            state: TASK_CREATED_STATE,
        };

        const tasks = await this.tasksRepository.createOne(taskDtoIn);

        const task = tasks[0];

        if (!task?.id) {
            throw new InternalServerErrorException();
        }

        if (project.state === PROJECT_CREATED_STATE) {
            const projectUpdateDtoIn = {
                id: project.id,
                version: project.version,
                updateData: {
                    state: PROJECT_ACTIVE_STATE,
                },
            } satisfies UpdateProjectDtoIn;

            await this.projectService.updateOneById(projectUpdateDtoIn);
        }

        const dtoOut = {
            data: task,
        };

        return dtoOut;
    }

    async updateOneById(dtoIn: UpdateTaskDtoIn): Promise<TaskDtoOut> {
        const { data: task } = await this.findOneById({ id: dtoIn.id });

        if (task.version !== dtoIn.version) {
            throw new BadRequestException({
                message: 'Incorrect version of document.',
                cause: { version: dtoIn.version },
            });
        }

        const filter = this.queryBuilder.buildUpdateOneQuery({ id: dtoIn.id });

        const tasks = await this.tasksRepository.updateOne(
            filter,
            dtoIn.updateData,
        );

        const updatedTask = tasks[0];

        if (!updatedTask?.id) {
            throw new InternalServerErrorException();
        }

        const dtoOut = {
            data: updatedTask,
        };

        return dtoOut;
    }

    async deleteOneById(dtoIn: DeleteTaskDtoIn): Promise<TaskDtoOut> {
        const { data: task } = await this.findOneById({ id: dtoIn.id });

        if (task.version !== dtoIn.version) {
            throw new BadRequestException({
                message: 'Incorrect version of document.',
                cause: { version: dtoIn.version },
            });
        }

        const filter = this.queryBuilder.buildDeleteOneQuery({ id: dtoIn.id });

        const tasks = await this.tasksRepository.deleteOne(filter);

        const deletedTask = tasks[0];

        if (!deletedTask?.id) {
            throw new InternalServerErrorException();
        }

        const dtoOut = {
            data: deletedTask,
        };

        return dtoOut;
    }
}
