import Joi from 'joi';

import { SORT_ORDER } from 'src/constants';

import {
    CreateTaskDtoIn,
    DeleteTaskDtoIn,
    FindManyTaskDtoIn,
    FindOneTaskDtoIn,
    UpdateTaskData,
    UpdateTaskDtoIn,
} from './tasks.model';
import {
    TASKS_PRIORITIES,
    TASKS_SORTABLE_FIELDS,
    TASKS_STATES,
} from './tasks.constants';

export const findManyTasksSchema = Joi.object<FindManyTaskDtoIn>({
    search: Joi.string().trim().min(1).max(255).optional(),
    state: Joi.string()
        .trim()
        .valid(...TASKS_STATES)
        .optional(),
    priority: Joi.string()
        .trim()
        .valid(...TASKS_PRIORITIES)
        .optional(),
    sortBy: Joi.string()
        .trim()
        .valid(...TASKS_SORTABLE_FIELDS)
        .optional(),
    sortOrder: Joi.string()
        .trim()
        .valid(...SORT_ORDER)
        .optional(),
    page: Joi.number().integer().positive().optional(),
    pageSize: Joi.number().integer().positive().optional(),
}).optional();

export const findOneByIdTaskSchema = Joi.object<FindOneTaskDtoIn, true>({
    id: Joi.number().integer().positive().optional(),
}).required();

export const createTaskSchema = Joi.object<CreateTaskDtoIn, true>({
    title: Joi.string().trim().min(1).max(255).required(),
    description: Joi.string().trim().min(1).max(3000).required(),
    priority: Joi.string()
        .trim()
        .valid(...TASKS_PRIORITIES)
        .required(),
    project_id: Joi.number().integer().positive().required(),
}).required();

export const updateOneTaskSchema = Joi.object<
    Omit<UpdateTaskDtoIn, 'id'>,
    true
>({
    version: Joi.number().integer().positive().required(),
    updateData: Joi.object<UpdateTaskData>({
        title: Joi.string().trim().min(1).max(255).optional(),
        description: Joi.string().trim().min(1).max(3000).optional(),
        state: Joi.string()
            .trim()
            .valid(...TASKS_STATES)
            .optional(),
        priority: Joi.string()
            .trim()
            .valid(...TASKS_PRIORITIES)
            .optional(),
    })
        .min(1)
        .required(),
}).required();

export const deleteOneTaskSchema = Joi.object<
    Omit<DeleteTaskDtoIn, 'id'>,
    true
>({
    version: Joi.number().integer().positive().required(),
}).required();
