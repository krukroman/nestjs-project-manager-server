import { TestBed } from '@automock/jest';
import {
    BadRequestException,
    InternalServerErrorException,
    NotFoundException,
} from '@nestjs/common';
import { sql, SQLWrapper } from 'drizzle-orm';

import { ProjectDtoOut } from 'src/app/projects/projects.model';
import { ProjectsService } from 'src/app/projects/projects.service';
import { getErrorAsync, NoErrorThrownError } from 'test/test-utils';

import {
    CreateTaskDtoIn,
    FindOneTaskDtoIn,
    Task,
    TaskDtoOut,
} from './tasks.model';
import { TasksQueryBuilder } from './tasks.query-builder';
import { TasksRepository } from './tasks.repository';
import { TasksService } from './tasks.service';

describe('TasksService', () => {
    let service: TasksService;

    beforeEach(async () => {
        const { unit } = TestBed.create(TasksService).compile();

        service = unit;
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});

describe('findMany', () => {
    let service: TasksService;
    let queryBuilder: jest.Mocked<TasksQueryBuilder>;
    let repository: jest.Mocked<TasksRepository>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksService).compile();

        service = unit;
        queryBuilder = unitRef.get(TasksQueryBuilder);
        repository = unitRef.get(TasksRepository);
    });

    it('should return a result from repository.', async function () {
        const mockedResult: Task[] = [];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = {};
        queryBuilder.buildFindManyQuery.mockReturnValue(mockedFilter);
        repository.findMany.mockResolvedValue(mockedResult);

        const { data } = await service.findMany(dtoIn);
        expect(data).toStrictEqual(mockedResult);
        expect(queryBuilder.buildFindManyQuery).toHaveBeenCalledTimes(1);
        expect(repository.findMany).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when dependencies thrown an error.', async function () {
        const mockedError = new Error('some error.');
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = {};
        queryBuilder.buildFindManyQuery.mockReturnValue(mockedFilter);
        repository.findMany.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.findMany(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(queryBuilder.buildFindManyQuery).toHaveBeenCalledTimes(1);
        expect(repository.findMany).toHaveBeenCalledTimes(1);
    });
});

describe('findOneById', () => {
    let service: TasksService;
    let queryBuilder: jest.Mocked<TasksQueryBuilder>;
    let repository: jest.Mocked<TasksRepository>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksService).compile();

        service = unit;
        queryBuilder = unitRef.get(TasksQueryBuilder);
        repository = unitRef.get(TasksRepository);
    });

    it('should return a result from repository.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 2',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = { id: 1 };
        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);

        const { data } = await service.findOneById(dtoIn);
        expect(data).toStrictEqual(mockedResult[0]);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when dependencies thrown an error.', async function () {
        const mockedError = new Error('some error.');
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = { id: 1 };
        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.findOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when task not found.', async function () {
        const mockedResult: Task[] = [];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = { id: 1 };
        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.findOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });
});

describe('createOne', () => {
    let service: TasksService;
    let projectsService: jest.Mocked<ProjectsService>;
    let repository: jest.Mocked<TasksRepository>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksService).compile();

        service = unit;
        projectsService = unitRef.get(ProjectsService);
        repository = unitRef.get(TasksRepository);
    });

    it('should return a result from repository.', async function () {
        const mockedProjectResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        projectsService.findOneById.mockResolvedValue(mockedProjectResult);
        repository.createOne.mockResolvedValue(mockedResult);

        const { data } = await service.createOne(dtoIn);
        expect(data).toStrictEqual(mockedResult[0]);
        expect(projectsService.findOneById).toHaveBeenCalledTimes(1);
        expect(repository.createOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when dependencies thrown an error.', async function () {
        const mockedProjectResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const mockedError = new Error('some error.');
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        projectsService.findOneById.mockResolvedValue(mockedProjectResult);
        repository.createOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(projectsService.findOneById).toHaveBeenCalledTimes(1);
        expect(repository.createOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when project not found.', async function () {
        const mockedError = new NotFoundException();
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        projectsService.findOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(projectsService.findOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when project has draft state.', async function () {
        const mockedProjectResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'draft',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        projectsService.findOneById.mockResolvedValue(mockedProjectResult);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(BadRequestException);
        expect(projectsService.findOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when project has inactive state.', async function () {
        const mockedProjectResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'archived',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        projectsService.findOneById.mockResolvedValue(mockedProjectResult);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(BadRequestException);
        expect(projectsService.findOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when repository returns an empty array.', async function () {
        const mockedProjectResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const mockedResult: Task[] = [];
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        projectsService.findOneById.mockResolvedValue(mockedProjectResult);
        repository.createOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(projectsService.findOneById).toHaveBeenCalledTimes(1);
        expect(repository.createOne).toHaveBeenCalledTimes(1);
    });
});

describe('updateOneById', () => {
    let service: TasksService;
    let queryBuilder: jest.Mocked<TasksQueryBuilder>;
    let repository: jest.Mocked<TasksRepository>;
    let findOneById: jest.SpyInstance<
        Promise<TaskDtoOut>,
        [dtoIn: FindOneTaskDtoIn],
        any
    >;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksService).compile();

        service = unit;
        queryBuilder = unitRef.get(TasksQueryBuilder);
        repository = unitRef.get(TasksRepository);
        findOneById = jest.spyOn(service, 'findOneById');
    });

    afterEach(function () {
        findOneById.mockClear();
    });

    afterAll(function () {
        findOneById.mockReset();
    });

    it('should return result from repository.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedUpdatedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 2',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 2,
            },
        ];
        const mockedFilter: SQLWrapper[] = [sql`"id" = 1`];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };
        queryBuilder.buildUpdateOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);
        repository.updateOne.mockResolvedValue(mockedUpdatedResult);

        const { data } = await service.updateOneById(dtoIn);
        expect(data).toStrictEqual(mockedUpdatedResult[0]);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildUpdateOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.updateOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when dependencies thrown an error.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedError = new Error('some error.');
        const mockedFilter: SQLWrapper[] = [sql`"id" = 1`];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };

        queryBuilder.buildUpdateOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);
        repository.updateOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildUpdateOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.updateOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when task not found.', async function () {
        const mockedResult: Task[] = [];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };

        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when version in dtoIn is not equal project version.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 2,
            updateData: {
                description: 'some description 2',
            },
        };

        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(BadRequestException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when repository returns an empty result.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedUpdatedResult: Task[] = [];
        const mockedFilter: SQLWrapper[] = [sql`"id" = 1`];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };

        queryBuilder.buildUpdateOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);
        repository.updateOne.mockResolvedValue(mockedUpdatedResult);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildUpdateOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.updateOne).toHaveBeenCalledTimes(1);
    });
});

describe('deleteOneById', () => {
    let service: TasksService;
    let queryBuilder: jest.Mocked<TasksQueryBuilder>;
    let repository: jest.Mocked<TasksRepository>;
    let findOneById: jest.SpyInstance<
        Promise<TaskDtoOut>,
        [dtoIn: FindOneTaskDtoIn],
        any
    >;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksService).compile();

        service = unit;
        queryBuilder = unitRef.get(TasksQueryBuilder);
        repository = unitRef.get(TasksRepository);
        findOneById = jest.spyOn(service, 'findOneById');
    });

    afterEach(function () {
        findOneById.mockClear();
    });

    afterAll(function () {
        findOneById.mockReset();
    });

    it('should return result from repository.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedDeletedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedFilter: SQLWrapper[] = [sql`"id" = 1`];
        const dtoIn = {
            id: 1,
            version: 1,
        };
        queryBuilder.buildDeleteOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);
        repository.deleteOne.mockResolvedValue(mockedDeletedResult);

        const { data } = await service.deleteOneById(dtoIn);
        expect(data).toStrictEqual(mockedDeletedResult[0]);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildDeleteOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.deleteOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when dependencies thrown an error.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedError = new Error('some error.');
        const mockedFilter: SQLWrapper[] = [sql`"id" = 1`];
        const dtoIn = {
            id: 1,
            version: 1,
        };

        queryBuilder.buildDeleteOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);
        repository.deleteOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildDeleteOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.deleteOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when task not found.', async function () {
        const mockedResult: Task[] = [];
        const dtoIn = {
            id: 1,
            version: 1,
        };

        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when version in dtoIn is not equal project version.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 2,
        };

        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(BadRequestException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when repository returns an empty result.', async function () {
        const mockedResult: Task[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedDeletedResult: Task[] = [];
        const mockedFilter: SQLWrapper[] = [sql`"id" = 1`];
        const dtoIn = {
            id: 1,
            version: 1,
        };

        queryBuilder.buildDeleteOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);
        repository.deleteOne.mockResolvedValue(mockedDeletedResult);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildDeleteOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.deleteOne).toHaveBeenCalledTimes(1);
    });
});
