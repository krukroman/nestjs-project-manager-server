import { tasks } from 'src/database/database.schema';
import { EntityDtoOut, PageInfo, SortOrder } from 'src/types';

import {
    TASKS_SORTABLE_FIELDS,
    TASKS_STATES,
    TASKS_UPDATABLE_STATES,
    TASKS_PRIORITIES,
} from './tasks.constants';

export type Task = typeof tasks.$inferSelect;

export type TaskInsert = typeof tasks.$inferInsert;

export type TaskState = (typeof TASKS_STATES)[number];
export type TaskPriority = (typeof TASKS_PRIORITIES)[number];
export type TaskUpdatableState = (typeof TASKS_UPDATABLE_STATES)[number];
export type TaskSortableField = (typeof TASKS_SORTABLE_FIELDS)[number];

export type TaskDtoOut = EntityDtoOut<Task>;

export type TasksDtoOut = EntityDtoOut<Task[]>;

export type CreateTaskDtoIn = {
    title: string;
    description: string;
    priority: TaskPriority;
    project_id: number;
};

export type UpdateTaskData = {
    title?: string;
    description?: string;
    state?: TaskUpdatableState;
    priority?: TaskPriority;
};

export type UpdateTaskDtoIn = {
    id: number;
    version: number;
    updateData: UpdateTaskData;
};

export type DeleteTaskDtoIn = {
    id: number;
    version: number;
};

export type FindOneTaskDtoIn = {
    id: number;
};

export type TaskSortObject = {
    sortBy: TaskSortableField;
    sortOrder: SortOrder;
};

export type FindManyTaskDtoIn = {
    search?: string;
    state?: TaskState;
    priority?: TaskPriority;
    project_id?: number;
} & Partial<TaskSortObject> &
    Partial<PageInfo>;
