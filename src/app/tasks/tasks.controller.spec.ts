import { TestBed } from '@automock/jest';

import { getErrorAsync, NoErrorThrownError } from 'test/test-utils';

import { TasksController } from './tasks.controller';
import { CreateTaskDtoIn, TaskDtoOut, TasksDtoOut } from './tasks.model';
import { TasksService } from './tasks.service';

describe('TasksController', () => {
    let controller: TasksController;

    beforeEach(() => {
        const { unit } = TestBed.create(TasksController).compile();

        controller = unit;
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});

describe('findMany', () => {
    let controller: TasksController;
    let service: jest.Mocked<TasksService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksController).compile();

        controller = unit;
        service = unitRef.get(TasksService);
    });

    it('should return result from service.', async function () {
        const mockedResult: TasksDtoOut = {
            data: [],
        };
        const dtoIn = {};
        service.findMany.mockResolvedValue(mockedResult);

        const result = await controller.findMany(dtoIn);
        expect(result).toStrictEqual(mockedResult);
        expect(service.findMany).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when service throws an error.', async function () {
        const dtoIn = {};
        const mockedError = new Error('some error');
        service.findMany.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.findMany(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.findMany).toHaveBeenCalledTimes(1);
    });
});

describe('findOneById', () => {
    let controller: TasksController;
    let service: jest.Mocked<TasksService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksController).compile();

        controller = unit;
        service = unitRef.get(TasksService);
    });

    it('should return result from service.', async function () {
        const mockedResult: TaskDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const dtoIn = { id: 1 };
        service.findOneById.mockResolvedValue(mockedResult);

        const result = await controller.findOneById(dtoIn);
        expect(result).toStrictEqual(mockedResult);
        expect(service.findOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when service throws an error.', async function () {
        const dtoIn = { id: 1 };
        const mockedError = new Error('some error');
        service.findOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.findOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.findOneById).toHaveBeenCalledTimes(1);
    });
});

describe('createOne', () => {
    let controller: TasksController;
    let service: jest.Mocked<TasksService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksController).compile();

        controller = unit;
        service = unitRef.get(TasksService);
    });

    it('should return result from service.', async function () {
        const mockedResult: TaskDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        service.createOne.mockResolvedValue(mockedResult);

        const result = await controller.createOne(dtoIn);
        expect(result).toStrictEqual(mockedResult);
        expect(service.createOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when service throws an error.', async function () {
        const dtoIn = {
            title: 'some title',
            description: 'some description',
            priority: 'A',
            project_id: 1,
        } satisfies CreateTaskDtoIn;
        const mockedError = new Error('some error');
        service.createOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.createOne).toHaveBeenCalledTimes(1);
    });
});

describe('updateOneById', () => {
    let controller: TasksController;
    let service: jest.Mocked<TasksService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksController).compile();

        controller = unit;
        service = unitRef.get(TasksService);
    });

    it('should return result from service.', async function () {
        const mockedResult: TaskDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description 2',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 2,
            },
        };
        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };
        service.updateOneById.mockResolvedValue(mockedResult);

        const result = await controller.updateOneById(params, dtoIn);
        expect(result).toStrictEqual(mockedResult);
        expect(service.updateOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when service throws an error.', async function () {
        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };
        const mockedError = new Error('some error');
        service.updateOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.updateOneById(params, dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.updateOneById).toHaveBeenCalledTimes(1);
    });
});

describe('deleteOneById', () => {
    let controller: TasksController;
    let service: jest.Mocked<TasksService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(TasksController).compile();

        controller = unit;
        service = unitRef.get(TasksService);
    });

    it('should return result from service.', async function () {
        const mockedResult: TaskDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description 1',
                project_id: 1,
                state: 'created',
                priority: 'A',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
        };
        service.deleteOneById.mockResolvedValue(mockedResult);

        const result = await controller.deleteOneById(params, dtoIn);
        expect(result).toStrictEqual(mockedResult);
        expect(service.deleteOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error when service throws an error.', async function () {
        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
        };
        const mockedError = new Error('some error');
        service.deleteOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.deleteOneById(params, dtoIn);
        };

        const error = await getErrorAsync(testFn);
        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.deleteOneById).toHaveBeenCalledTimes(1);
    });
});
