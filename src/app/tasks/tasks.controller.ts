import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Post,
    Put,
    Query,
} from '@nestjs/common';

import { JoiValidationPipe } from 'src/pipes/joi-validation.pipe';

import {
    CreateTaskDtoIn,
    DeleteTaskDtoIn,
    FindManyTaskDtoIn,
    FindOneTaskDtoIn,
    TaskDtoOut,
    TasksDtoOut,
    UpdateTaskDtoIn,
} from './tasks.model';
import { TasksService } from './tasks.service';
import {
    createTaskSchema,
    deleteOneTaskSchema,
    findManyTasksSchema,
    findOneByIdTaskSchema,
    updateOneTaskSchema,
} from './tasks.validation-schema';

@Controller('tasks')
export class TasksController {
    constructor(private readonly tasksService: TasksService) {}

    @HttpCode(HttpStatus.OK)
    @Get()
    async findMany(
        @Query(new JoiValidationPipe(findManyTasksSchema))
        query: FindManyTaskDtoIn,
    ): Promise<TasksDtoOut> {
        return await this.tasksService.findMany(query);
    }

    @HttpCode(HttpStatus.OK)
    @Get('/:id')
    async findOneById(
        @Param(new JoiValidationPipe(findOneByIdTaskSchema))
        params: FindOneTaskDtoIn,
    ): Promise<TaskDtoOut> {
        return await this.tasksService.findOneById(params);
    }

    @HttpCode(HttpStatus.CREATED)
    @Post()
    async createOne(
        @Body(new JoiValidationPipe(createTaskSchema)) body: CreateTaskDtoIn,
    ): Promise<TaskDtoOut> {
        return await this.tasksService.createOne(body);
    }

    @HttpCode(HttpStatus.OK)
    @Put('/:id')
    async updateOneById(
        @Param(new JoiValidationPipe(findOneByIdTaskSchema))
        params: FindOneTaskDtoIn,
        @Body(new JoiValidationPipe(updateOneTaskSchema))
        body: Omit<UpdateTaskDtoIn, 'id'>,
    ): Promise<TaskDtoOut> {
        const dtoIn = {
            ...params,
            ...body,
        } satisfies UpdateTaskDtoIn;

        return await this.tasksService.updateOneById(dtoIn);
    }

    @HttpCode(HttpStatus.OK)
    @Delete('/:id')
    async deleteOneById(
        @Param(new JoiValidationPipe(findOneByIdTaskSchema))
        params: FindOneTaskDtoIn,
        @Body(new JoiValidationPipe(deleteOneTaskSchema))
        body: Omit<DeleteTaskDtoIn, 'id'>,
    ): Promise<TaskDtoOut> {
        const dtoIn = {
            ...params,
            ...body,
        } satisfies DeleteTaskDtoIn;

        return await this.tasksService.deleteOneById(dtoIn);
    }
}
