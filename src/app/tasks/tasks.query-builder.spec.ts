import { and, eq, ilike, inArray, or, SQLWrapper } from 'drizzle-orm';

import * as schema from 'src/database/database.schema';
import { getErrorSync, NoErrorThrownError } from 'test/test-utils';

import { TASKS_ACTIVE_STATES } from './tasks.constants';
import { TasksQueryBuilder } from './tasks.query-builder';

describe('TasksQueryBuilder', function () {
    it('should be defined.', function () {
        expect(TasksQueryBuilder).toBeDefined();
    });
});

describe('buildFindManyQuery', function () {
    const builder = new TasksQueryBuilder();

    it('should return a result when dtoIn is empty.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [
            and(inArray(schema.tasks.state, TASKS_ACTIVE_STATES as any)),
        ];

        const result = builder.buildFindManyQuery({});

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return result with title and description in or clause.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [
            or(
                ilike(schema.tasks.title, '%text%'),
                ilike(schema.tasks.description, '%text%'),
            ),
            and(inArray(schema.tasks.state, TASKS_ACTIVE_STATES as any)),
        ];

        const result = builder.buildFindManyQuery({
            title: 'text',
            description: 'text',
        });

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return result with title and description in or clause and other entries in and clause.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [
            or(
                ilike(schema.tasks.title, '%text%'),
                ilike(schema.tasks.description, '%text%'),
            ),
            and(
                eq(schema.tasks.id, 1),
                eq(schema.tasks.state, 'completed'),
                eq(schema.tasks.priority, 'A'),
                eq(schema.tasks.project_id, 1),
            ),
        ];

        const result = builder.buildFindManyQuery({
            id: 1,
            title: 'text',
            description: 'text',
            state: 'completed',
            priority: 'A',
            project_id: 1,
        });

        expect(result).toStrictEqual(expectedResult);
    });
});

describe('buildFindOneQuery', function () {
    const builder = new TasksQueryBuilder();

    it('should return a result when dtoIn is empty.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [];

        const result = builder.buildFindOneQuery({});

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return result if dtoIn is not empty.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [
            eq(schema.tasks.id, 1),
            eq(schema.tasks.title, 'text'),
            eq(schema.tasks.description, 'text'),
            eq(schema.tasks.state, 'completed'),
            eq(schema.tasks.priority, 'A'),
            eq(schema.tasks.project_id, 1),
        ];

        const result = builder.buildFindOneQuery({
            id: 1,
            title: 'text',
            description: 'text',
            state: 'completed',
            priority: 'A',
            project_id: 1,
        });

        expect(result).toStrictEqual(expectedResult);
    });
});

describe('buildUpdateOneQuery', function () {
    const builder = new TasksQueryBuilder();

    it('should throw an error when dtoIn is empty.', function () {
        const testFn = function () {
            builder.buildUpdateOneQuery({});
        };

        const error = getErrorSync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toHaveProperty(
            'message',
            'dtoIn should have at least one key-value pair.',
        );
    });

    it('should return result if dtoIn is not empty.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [
            eq(schema.tasks.id, 1),
            eq(schema.tasks.title, 'text'),
            eq(schema.tasks.description, 'text'),
            eq(schema.tasks.state, 'completed'),
            eq(schema.tasks.priority, 'A'),
            eq(schema.tasks.project_id, 1),
        ];

        const result = builder.buildUpdateOneQuery({
            id: 1,
            title: 'text',
            description: 'text',
            state: 'completed',
            priority: 'A',
            project_id: 1,
        });

        expect(result).toStrictEqual(expectedResult);
    });
});

describe('buildDeleteOneQuery', function () {
    const builder = new TasksQueryBuilder();

    it('should throw an error when dtoIn is empty.', function () {
        const testFn = function () {
            builder.buildDeleteOneQuery({});
        };

        const error = getErrorSync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toHaveProperty(
            'message',
            'dtoIn should have at least one key-value pair.',
        );
    });

    it('should return result if dtoIn is not empty.', function () {
        const expectedResult: (SQLWrapper | undefined)[] = [
            eq(schema.tasks.id, 1),
            eq(schema.tasks.title, 'text'),
            eq(schema.tasks.description, 'text'),
            eq(schema.tasks.state, 'completed'),
            eq(schema.tasks.priority, 'A'),
            eq(schema.tasks.project_id, 1),
        ];

        const result = builder.buildDeleteOneQuery({
            id: 1,
            title: 'text',
            description: 'text',
            state: 'completed',
            priority: 'A',
            project_id: 1,
        });

        expect(result).toStrictEqual(expectedResult);
    });
});
