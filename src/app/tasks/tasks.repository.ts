import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { and, asc, desc, SQLWrapper } from 'drizzle-orm';

import { DESC } from 'src/constants';
import * as schema from 'src/database/database.schema';
import { DrizzleService } from 'src/database/drizzle/drizzle.service';
import { PageInfo } from 'src/types';

import { Task, TaskInsert, TaskSortObject } from './tasks.model';

@Injectable()
export class TasksRepository {
    constructor(private readonly drizzleService: DrizzleService) {}

    async findMany(
        filter: (SQLWrapper | undefined)[],
        sortObject: TaskSortObject,
        pageInfo: PageInfo,
    ): Promise<Task[]> {
        try {
            const orderBy =
                sortObject.sortOrder === DESC
                    ? desc(schema.tasks[sortObject.sortBy])
                    : asc(schema.tasks[sortObject.sortBy]);
            const offset = (pageInfo.page - 1) * pageInfo.pageSize;
            const limit = pageInfo.pageSize;

            const tasks = await this.drizzleService.db
                .select()
                .from(schema.tasks)
                .where(and(...filter))
                .orderBy(orderBy)
                .offset(offset)
                .limit(limit);

            return tasks;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async findOne(filter: (SQLWrapper | undefined)[]): Promise<Task[]> {
        try {
            const tasks = await this.drizzleService.db
                .select()
                .from(schema.tasks)
                .where(and(...filter))
                .limit(1);
            return tasks;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async createOne(dtoIn: TaskInsert): Promise<Task[]> {
        try {
            const tasks = await this.drizzleService.db
                .insert(schema.tasks)
                .values(dtoIn)
                .returning();

            return tasks;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async updateOne(
        filter: SQLWrapper[],
        dtoIn: Partial<Task>,
    ): Promise<Task[]> {
        try {
            const tasks = this.drizzleService.db
                .update(schema.tasks)
                .set(dtoIn)
                .where(and(...filter))
                .returning();

            return tasks;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async deleteOne(filter: SQLWrapper[]) {
        try {
            const tasks = await this.drizzleService.db
                .delete(schema.tasks)
                .where(and(...filter))
                .returning();

            return tasks;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }
}
