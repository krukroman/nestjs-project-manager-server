import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { DatabaseModule } from 'src/database/database.module';
import { databaseOptions } from 'src/database/database.options';
import { envSchema } from 'src/validation-schemas';

import { ProjectsModule } from './projects/projects.module';
import { TasksModule } from './tasks/tasks.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
            validationSchema: envSchema,
            validationOptions: {
                abortEarly: false,
            },
        }),
        DatabaseModule.forRootAsync(databaseOptions),
        ProjectsModule,
        TasksModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
