import { TestBed } from '@automock/jest';

import { getErrorAsync, NoErrorThrownError } from 'test/test-utils';

import { ProjectsController } from './projects.controller';
import { ProjectDtoOut, ProjectsDtoOut } from './projects.model';
import { ProjectsService } from './projects.service';

describe('ProjectsController', () => {
    let controller: ProjectsController;

    beforeEach(() => {
        const { unit } = TestBed.create(ProjectsController).compile();
        controller = unit;
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});

describe('findMany', function () {
    let controller: ProjectsController;
    let service: jest.Mocked<ProjectsService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(ProjectsController).compile();
        controller = unit;
        service = unitRef.get(ProjectsService);
    });

    it('should return result of service.', async function () {
        const mockedResult: ProjectsDtoOut = {
            data: [],
        };
        const dtoIn = {};
        service.findMany.mockResolvedValue(mockedResult);

        const result = await controller.findMany(dtoIn);
        expect(service.findMany).toHaveBeenCalledTimes(1);
        expect(result).toStrictEqual(mockedResult);
    });

    it('should throw an error if service throws an error.', async function () {
        const mockedError = new Error('some error');
        const dtoIn = {};
        service.findMany.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.findMany(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.findMany).toHaveBeenCalledTimes(1);
    });
});

describe('findOneById', function () {
    let controller: ProjectsController;
    let service: jest.Mocked<ProjectsService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(ProjectsController).compile();
        controller = unit;
        service = unitRef.get(ProjectsService);
    });

    it('should return result of service.', async function () {
        const mockedResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const dtoIn = { id: 1 };
        service.findOneById.mockResolvedValue(mockedResult);

        const result = await controller.findOneById(dtoIn);
        expect(service.findOneById).toHaveBeenCalledTimes(1);
        expect(result).toStrictEqual(mockedResult);
    });

    it('should throw an error if service throws an error.', async function () {
        const mockedError = new Error('some error');
        const dtoIn = { id: 1 };
        service.findOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.findOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.findOneById).toHaveBeenCalledTimes(1);
    });
});

describe('createOne', function () {
    let controller: ProjectsController;
    let service: jest.Mocked<ProjectsService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(ProjectsController).compile();
        controller = unit;
        service = unitRef.get(ProjectsService);
    });

    it('should return result of service.', async function () {
        const mockedResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };
        const dtoIn = { title: 'some title', description: 'some description' };
        service.createOne.mockResolvedValue(mockedResult);

        const result = await controller.createOne(dtoIn);
        expect(service.createOne).toHaveBeenCalledTimes(1);
        expect(result).toStrictEqual(mockedResult);
    });

    it('should throw an error if service throws an error.', async function () {
        const mockedError = new Error('some error');
        const dtoIn = { title: 'some title', description: 'some description' };
        service.createOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.createOne).toHaveBeenCalledTimes(1);
    });
});

describe('updateOneById', function () {
    let controller: ProjectsController;
    let service: jest.Mocked<ProjectsService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(ProjectsController).compile();
        controller = unit;
        service = unitRef.get(ProjectsService);
    });

    it('should return result of service.', async function () {
        const mockedResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };

        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
            updateData: {
                description: 'some description',
            },
        };
        service.updateOneById.mockResolvedValue(mockedResult);

        const result = await controller.updateOneById(params, dtoIn);
        expect(service.updateOneById).toHaveBeenCalledTimes(1);
        expect(result).toStrictEqual(mockedResult);
    });

    it('should throw an error if service throws an error.', async function () {
        const mockedError = new Error('some error');
        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
            updateData: {
                description: 'some description',
            },
        };
        service.updateOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.updateOneById(params, dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.updateOneById).toHaveBeenCalledTimes(1);
    });
});

describe('deleteOneById', function () {
    let controller: ProjectsController;
    let service: jest.Mocked<ProjectsService>;

    beforeEach(() => {
        const { unit, unitRef } = TestBed.create(ProjectsController).compile();
        controller = unit;
        service = unitRef.get(ProjectsService);
    });

    it('should return result of service.', async function () {
        const mockedResult: ProjectDtoOut = {
            data: {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        };

        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
        };
        service.deleteOneById.mockResolvedValue(mockedResult);

        const result = await controller.deleteOneById(params, dtoIn);
        expect(service.deleteOneById).toHaveBeenCalledTimes(1);
        expect(result).toStrictEqual(mockedResult);
    });

    it('should throw an error if service throws an error.', async function () {
        const mockedError = new Error('some error');
        const params = {
            id: 1,
        };
        const dtoIn = {
            version: 1,
        };
        service.deleteOneById.mockRejectedValue(mockedError);

        const testFn = async function () {
            await controller.deleteOneById(params, dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(service.deleteOneById).toHaveBeenCalledTimes(1);
    });
});
