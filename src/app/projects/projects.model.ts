import { projects } from 'src/database/database.schema';
import { EntityDtoOut, PageInfo, SortOrder } from 'src/types';

import {
    PROJECT_DRAFT_STATE,
    PROJECTS_SORTABLE_FIELDS,
    PROJECTS_STATES,
    PROJECTS_UPDATABLE_STATES,
} from './projects.constants';

export type Project = typeof projects.$inferSelect;

export type ProjectInsert = typeof projects.$inferInsert;

export type ProjectDraftState = typeof PROJECT_DRAFT_STATE;
export type ProjectState = (typeof PROJECTS_STATES)[number];
export type ProjectUpdatableState = (typeof PROJECTS_UPDATABLE_STATES)[number];
export type ProjectSortableField = (typeof PROJECTS_SORTABLE_FIELDS)[number];

export type ProjectDtoOut = EntityDtoOut<Project>;

export type ProjectsDtoOut = EntityDtoOut<Project[]>;

export type CreateProjectDtoIn = {
    title: string;
    description: string;
    state?: ProjectDraftState;
};

export type UpdateProjectData = {
    title?: string;
    description?: string;
    state?: ProjectUpdatableState;
};

export type UpdateProjectDtoIn = {
    id: number;
    version: number;
    updateData: UpdateProjectData;
};

export type DeleteProjectDtoIn = {
    id: number;
    version: number;
};

export type FindOneProjectDtoIn = {
    id: number;
};

export type ProjectSortObject = {
    sortBy: ProjectSortableField;
    sortOrder: SortOrder;
};

export type FindManyProjectDtoIn = {
    search?: string;
    state?: ProjectState;
} & Partial<ProjectSortObject> &
    Partial<PageInfo>;
