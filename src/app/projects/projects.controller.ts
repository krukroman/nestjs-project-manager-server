import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Post,
    Put,
    Query,
} from '@nestjs/common';

import { JoiValidationPipe } from 'src/pipes/joi-validation.pipe';

import {
    CreateProjectDtoIn,
    DeleteProjectDtoIn,
    FindManyProjectDtoIn,
    FindOneProjectDtoIn,
    ProjectDtoOut,
    ProjectsDtoOut,
    UpdateProjectDtoIn,
} from './projects.model';
import { ProjectsService } from './projects.service';
import {
    createProjectSchema,
    deleteOneProjectSchema,
    findManyProjectsSchema,
    findOneByIdProjectSchema,
    updateOneProjectSchema,
} from './projects.validation-schema';

@Controller('projects')
export class ProjectsController {
    constructor(private readonly projectsService: ProjectsService) {}

    @Get()
    @HttpCode(HttpStatus.OK)
    async findMany(
        @Query(new JoiValidationPipe(findManyProjectsSchema))
        query: FindManyProjectDtoIn,
    ): Promise<ProjectsDtoOut> {
        return await this.projectsService.findMany(query);
    }

    @Get('/:id')
    @HttpCode(HttpStatus.OK)
    async findOneById(
        @Param(new JoiValidationPipe(findOneByIdProjectSchema))
        params: FindOneProjectDtoIn,
    ): Promise<ProjectDtoOut> {
        return await this.projectsService.findOneById(params);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    async createOne(
        @Body(new JoiValidationPipe(createProjectSchema))
        body: CreateProjectDtoIn,
    ): Promise<ProjectDtoOut> {
        return await this.projectsService.createOne(body);
    }

    @Put('/:id')
    @HttpCode(HttpStatus.OK)
    async updateOneById(
        @Param(new JoiValidationPipe(findOneByIdProjectSchema))
        params: FindOneProjectDtoIn,
        @Body(new JoiValidationPipe(updateOneProjectSchema))
        body: Omit<UpdateProjectDtoIn, 'id'>,
    ): Promise<ProjectDtoOut> {
        const dtoIn = { ...params, ...body } satisfies UpdateProjectDtoIn;
        return await this.projectsService.updateOneById(dtoIn);
    }

    @Delete('/:id')
    @HttpCode(HttpStatus.OK)
    async deleteOneById(
        @Param(new JoiValidationPipe(findOneByIdProjectSchema))
        params: FindOneProjectDtoIn,
        @Body(new JoiValidationPipe(deleteOneProjectSchema))
        body: Omit<DeleteProjectDtoIn, 'id'>,
    ): Promise<ProjectDtoOut> {
        const dtoIn = {
            ...params,
            ...body,
        } satisfies DeleteProjectDtoIn;

        return await this.projectsService.deleteOneById(dtoIn);
    }
}
