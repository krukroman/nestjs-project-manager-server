import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { SQLWrapper, and, asc, desc } from 'drizzle-orm';

import { DESC } from 'src/constants';
import * as schema from 'src/database/database.schema';
import { DrizzleService } from 'src/database/drizzle/drizzle.service';
import { PageInfo } from 'src/types';

import { Project, ProjectInsert, ProjectSortObject } from './projects.model';

@Injectable()
export class ProjectsRepository {
    constructor(private readonly drizzleService: DrizzleService) {}

    async findMany(
        filter: (SQLWrapper | undefined)[],
        sortObject: ProjectSortObject,
        pageInfo: PageInfo,
    ): Promise<Project[]> {
        const offset = (pageInfo.page - 1) * pageInfo.pageSize;
        const limit = pageInfo.pageSize;
        const orderBy =
            sortObject.sortOrder === DESC
                ? desc(schema.projects[sortObject.sortBy])
                : asc(schema.projects[sortObject.sortBy]);

        try {
            const projects = await this.drizzleService.db
                .select()
                .from(schema.projects)
                .where(and(...filter))
                .orderBy(orderBy)
                .offset(offset)
                .limit(limit);

            return projects;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async findOne(filter: (SQLWrapper | undefined)[]): Promise<Project[]> {
        try {
            const projects = await this.drizzleService.db
                .select()
                .from(schema.projects)
                .where(and(...filter))
                .limit(1);

            return projects;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async createOne(dtoIn: ProjectInsert): Promise<Project[]> {
        try {
            const projects = await this.drizzleService.db
                .insert(schema.projects)
                .values(dtoIn)
                .returning();

            return projects;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async updateOne(
        filter: SQLWrapper[],
        dtoIn: Partial<Project>,
    ): Promise<Project[]> {
        try {
            const projects = await this.drizzleService.db
                .update(schema.projects)
                .set(dtoIn)
                .where(and(...filter))
                .returning();

            return projects;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }

    async deleteOne(filter: SQLWrapper[]): Promise<Project[]> {
        try {
            const projects = await this.drizzleService.db
                .delete(schema.projects)
                .where(and(...filter))
                .returning();

            return projects;
        } catch (error) {
            throw new InternalServerErrorException(
                {
                    message: 'Internal server error.',
                },
                {
                    cause: error,
                },
            );
        }
    }
}
