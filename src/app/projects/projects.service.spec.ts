import { TestBed } from '@automock/jest';
import {
    BadRequestException,
    InternalServerErrorException,
    NotFoundException,
} from '@nestjs/common';
import { sql, SQLWrapper } from 'drizzle-orm';

import { getErrorAsync, NoErrorThrownError } from 'test/test-utils';

import { FindOneProjectDtoIn, Project, ProjectDtoOut } from './projects.model';
import { ProjectsQueryBuilder } from './projects.query-builder';
import { ProjectsRepository } from './projects.repository';
import { ProjectsService } from './projects.service';

describe('ProjectsService', () => {
    let service: ProjectsService;

    beforeEach(() => {
        const { unit } = TestBed.create(ProjectsService).compile();
        service = unit;
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});

describe('findMany', function () {
    let service: ProjectsService;
    let queryBuilder: jest.Mocked<ProjectsQueryBuilder>;
    let repository: jest.Mocked<ProjectsRepository>;

    beforeEach(function () {
        const { unit, unitRef } = TestBed.create(ProjectsService).compile();
        service = unit;
        queryBuilder = unitRef.get(ProjectsQueryBuilder);
        repository = unitRef.get(ProjectsRepository);
    });

    it('should return a defined result.', async function () {
        const mockedResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = {};
        queryBuilder.buildFindManyQuery.mockReturnValue(mockedFilter);
        repository.findMany.mockResolvedValue(mockedResult);

        const { data } = await service.findMany(dtoIn);
        expect(queryBuilder.buildFindManyQuery).toHaveBeenCalledTimes(1);
        expect(repository.findMany).toHaveBeenCalledTimes(1);
        expect(data).toStrictEqual(mockedResult);
    });

    it('should throw an error if dependencies throws errors.', async function () {
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = {};
        const mockedError = new Error('some error');
        queryBuilder.buildFindManyQuery.mockReturnValue(mockedFilter);
        repository.findMany.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.findMany(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(queryBuilder.buildFindManyQuery).toHaveBeenCalledTimes(1);
        expect(repository.findMany).toHaveBeenCalledTimes(1);
    });
});

describe('findOneById', function () {
    let service: ProjectsService;
    let queryBuilder: jest.Mocked<ProjectsQueryBuilder>;
    let repository: jest.Mocked<ProjectsRepository>;

    beforeEach(function () {
        const { unit, unitRef } = TestBed.create(ProjectsService).compile();
        service = unit;
        queryBuilder = unitRef.get(ProjectsQueryBuilder);
        repository = unitRef.get(ProjectsRepository);
    });

    it('should return a defined result.', async function () {
        const mockedResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = { id: 1 };
        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);

        const { data } = await service.findOneById(dtoIn);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
        expect(data).toStrictEqual(mockedResult[0]);
    });

    it('should throw an error if dependencies throws errors.', async function () {
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = { id: 1 };
        const mockedError = new Error('some error');
        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.findOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error project not found.', async function () {
        const mockedResult: Project[] = [];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = { id: 1 };
        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.findOneById(dtoIn);
        };
        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });
});

describe('createOne', function () {
    let service: ProjectsService;
    let repository: jest.Mocked<ProjectsRepository>;

    beforeEach(function () {
        const { unit, unitRef } = TestBed.create(ProjectsService).compile();
        service = unit;
        repository = unitRef.get(ProjectsRepository);
    });

    it('should return a defined result.', async function () {
        const mockedResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];

        const dtoIn = { title: 'some title', description: 'some description' };

        repository.createOne.mockResolvedValue(mockedResult);
        const { data } = await service.createOne(dtoIn);
        expect(repository.createOne).toHaveBeenCalledTimes(1);
        expect(data).toStrictEqual(mockedResult[0]);
    });

    it('should throw an error if dependencies throws errors.', async function () {
        const dtoIn = { title: 'some title', description: 'some description' };
        const mockedError = new Error('some error');
        repository.createOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(repository.createOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error repository returns empty value.', async function () {
        const mockedResult: Project[] = [];
        const dtoIn = { title: 'some title', description: 'some description' };

        repository.createOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.createOne(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(repository.createOne).toHaveBeenCalledTimes(1);
    });
});

describe('updateOneById', function () {
    let service: ProjectsService;
    let queryBuilder: jest.Mocked<ProjectsQueryBuilder>;
    let repository: jest.Mocked<ProjectsRepository>;
    let findOneById: jest.SpyInstance<
        Promise<ProjectDtoOut>,
        [dtoIn: FindOneProjectDtoIn],
        any
    >;

    beforeEach(function () {
        const { unit, unitRef } = TestBed.create(ProjectsService).compile();
        service = unit;
        queryBuilder = unitRef.get(ProjectsQueryBuilder);
        repository = unitRef.get(ProjectsRepository);
        findOneById = jest.spyOn(service, 'findOneById');
    });

    afterEach(function () {
        findOneById.mockClear();
    });

    afterAll(function () {
        findOneById.mockReset();
    });

    it('should return a defined result.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const mockedUpdateOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description 2',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 2,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };
        const mockedUpdateOneFilter: SQLWrapper[] = [sql`"id" = ${dtoIn.id}`];

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });
        queryBuilder.buildUpdateOneQuery.mockReturnValue(mockedUpdateOneFilter);
        repository.updateOne.mockResolvedValue(mockedUpdateOneResult);

        const { data } = await service.updateOneById(dtoIn);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildUpdateOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.updateOne).toHaveBeenCalledTimes(1);
        expect(data).toStrictEqual(mockedUpdateOneResult[0]);
    });

    it('should throw an error if dependencies throws errors.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };

        const mockedUpdateOneFilter: SQLWrapper[] = [sql`"id" = ${dtoIn.id}`];
        const mockedError = new Error('some error');

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });
        queryBuilder.buildUpdateOneQuery.mockReturnValue(mockedUpdateOneFilter);
        repository.updateOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildUpdateOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.updateOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error project not found.', async function () {
        const mockedResult: Project[] = [];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };

        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if version in dtoIn is not equal project version.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 3,
            updateData: {
                description: 'some description 2',
            },
        };

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(BadRequestException);
        expect(findOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if repository returns an empty value.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 1,
            updateData: {
                description: 'some description 2',
            },
        };

        const mockedUpdateOneFilter: SQLWrapper[] = [sql`"id" = ${dtoIn.id}`];

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });
        queryBuilder.buildUpdateOneQuery.mockReturnValue(mockedUpdateOneFilter);
        repository.updateOne.mockResolvedValue([]);

        const testFn = async function () {
            await service.updateOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildUpdateOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.updateOne).toHaveBeenCalledTimes(1);
    });
});

describe('deleteOneById', function () {
    let service: ProjectsService;
    let queryBuilder: jest.Mocked<ProjectsQueryBuilder>;
    let repository: jest.Mocked<ProjectsRepository>;
    let findOneById: jest.SpyInstance<
        Promise<ProjectDtoOut>,
        [dtoIn: FindOneProjectDtoIn],
        any
    >;

    beforeEach(function () {
        const { unit, unitRef } = TestBed.create(ProjectsService).compile();
        service = unit;
        queryBuilder = unitRef.get(ProjectsQueryBuilder);
        repository = unitRef.get(ProjectsRepository);
        findOneById = jest.spyOn(service, 'findOneById');
    });

    afterEach(function () {
        findOneById.mockClear();
    });

    afterAll(function () {
        findOneById.mockReset();
    });

    it('should return a defined result.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 1,
        };
        const mockedUpdateOneFilter: SQLWrapper[] = [sql`"id" = ${dtoIn.id}`];

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });
        queryBuilder.buildDeleteOneQuery.mockReturnValue(mockedUpdateOneFilter);
        repository.deleteOne.mockResolvedValue(mockedFindOneResult);

        const { data } = await service.deleteOneById(dtoIn);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildDeleteOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.deleteOne).toHaveBeenCalledTimes(1);
        expect(data).toStrictEqual(mockedFindOneResult[0]);
    });

    it('should throw an error if dependencies throws errors.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 1,
        };

        const mockedUpdateOneFilter: SQLWrapper[] = [sql`"id" = ${dtoIn.id}`];
        const mockedError = new Error('some error');

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });
        queryBuilder.buildDeleteOneQuery.mockReturnValue(mockedUpdateOneFilter);
        repository.deleteOne.mockRejectedValue(mockedError);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toStrictEqual(mockedError);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildDeleteOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.deleteOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error project not found.', async function () {
        const mockedResult: Project[] = [];
        const mockedFilter: (SQLWrapper | undefined)[] = [];
        const dtoIn = {
            id: 1,
            version: 1,
        };

        queryBuilder.buildFindOneQuery.mockReturnValue(mockedFilter);
        repository.findOne.mockResolvedValue(mockedResult);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(NotFoundException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildFindOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.findOne).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if version in dtoIn is not equal project version.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 3,
        };

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(BadRequestException);
        expect(findOneById).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if repository returns an empty value.', async function () {
        const mockedFindOneResult: Project[] = [
            {
                id: 1,
                title: 'some title',
                description: 'some description',
                state: 'created',
                created_at: new Date(),
                updated_at: new Date(),
                version: 1,
            },
        ];
        const dtoIn = {
            id: 1,
            version: 1,
        };

        const mockedUpdateOneFilter: SQLWrapper[] = [sql`"id" = ${dtoIn.id}`];

        findOneById.mockResolvedValue({ data: mockedFindOneResult[0]! });
        queryBuilder.buildDeleteOneQuery.mockReturnValue(mockedUpdateOneFilter);
        repository.deleteOne.mockResolvedValue([]);

        const testFn = async function () {
            await service.deleteOneById(dtoIn);
        };

        const error = await getErrorAsync(testFn);

        expect(error).not.toBeInstanceOf(NoErrorThrownError);
        expect(error).toBeInstanceOf(InternalServerErrorException);
        expect(findOneById).toHaveBeenCalledTimes(1);
        expect(queryBuilder.buildDeleteOneQuery).toHaveBeenCalledTimes(1);
        expect(repository.deleteOne).toHaveBeenCalledTimes(1);
    });
});
