import { DESC } from 'src/constants';

export const PROJECT_DRAFT_STATE = 'draft';
export const PROJECT_CREATED_STATE = 'created';
export const PROJECT_ACTIVE_STATE = 'active';
export const PROJECT_COMPLETED_STATE = 'completed';
export const PROJECT_ARCHIVED_STATE = 'archived';

export const PROJECTS_STATES = Object.freeze([
    PROJECT_DRAFT_STATE,
    PROJECT_CREATED_STATE,
    PROJECT_ACTIVE_STATE,
    PROJECT_COMPLETED_STATE,
    PROJECT_ARCHIVED_STATE,
] as const);

export const PROJECTS_ACTIVE_STATES = Object.freeze([
    PROJECT_CREATED_STATE,
    PROJECT_ACTIVE_STATE,
] as const);

export const PROJECTS_INACTIVE_STATES = Object.freeze([
    PROJECT_COMPLETED_STATE,
    PROJECT_ARCHIVED_STATE,
] as const);

export const PROJECTS_UPDATABLE_STATES = Object.freeze([
    PROJECT_CREATED_STATE,
    PROJECT_ACTIVE_STATE,
    PROJECT_COMPLETED_STATE,
    PROJECT_ARCHIVED_STATE,
] as const);

export const PROJECTS_SORTABLE_FIELDS = Object.freeze([
    'title',
    'state',
    'created_at',
] as const);

export const PROJECTS_DEFAULT_SORTABLE_FIELD = 'created_at';

export const PROJECTS_DEFAULT_SORT_ORDER = DESC;

export const PROJECTS_PAGE_SIZE_LIMIT = 300;

export const PROJECTS_DEFAULT_PAGE_INFO = Object.freeze({
    PAGE: 1,
    PAGE_SIZE: 100,
});
