import {
    BadRequestException,
    Injectable,
    InternalServerErrorException,
    NotFoundException,
} from '@nestjs/common';

import {
    PROJECT_CREATED_STATE,
    PROJECTS_DEFAULT_PAGE_INFO,
    PROJECTS_DEFAULT_SORT_ORDER,
    PROJECTS_DEFAULT_SORTABLE_FIELD,
    PROJECTS_PAGE_SIZE_LIMIT,
} from './projects.constants';
import {
    CreateProjectDtoIn,
    DeleteProjectDtoIn,
    FindManyProjectDtoIn,
    FindOneProjectDtoIn,
    Project,
    ProjectDtoOut,
    ProjectInsert,
    ProjectsDtoOut,
    ProjectSortObject,
    UpdateProjectDtoIn,
} from './projects.model';
import { ProjectsQueryBuilder } from './projects.query-builder';
import { ProjectsRepository } from './projects.repository';

@Injectable()
export class ProjectsService {
    constructor(
        private readonly projectsRepository: ProjectsRepository,
        private readonly queryBuilder: ProjectsQueryBuilder,
    ) {}

    async findMany(dtoIn: FindManyProjectDtoIn): Promise<ProjectsDtoOut> {
        const sortObject = {
            sortBy: dtoIn.sortBy || PROJECTS_DEFAULT_SORTABLE_FIELD,
            sortOrder: dtoIn.sortOrder || PROJECTS_DEFAULT_SORT_ORDER,
        } satisfies ProjectSortObject;

        const page = dtoIn.page || PROJECTS_DEFAULT_PAGE_INFO.PAGE;
        let pageSize = dtoIn.pageSize || PROJECTS_DEFAULT_PAGE_INFO.PAGE_SIZE;

        if (pageSize > PROJECTS_PAGE_SIZE_LIMIT) {
            pageSize = PROJECTS_PAGE_SIZE_LIMIT;
        }

        const pageInfo = {
            page,
            pageSize,
        };

        const projectsDtoIn: Partial<Project> = {};

        if (dtoIn.search) {
            projectsDtoIn.title = dtoIn.search;
            projectsDtoIn.description = dtoIn.search;
        }

        if (dtoIn.state) {
            projectsDtoIn.state = dtoIn.state;
        }

        const filter = this.queryBuilder.buildFindManyQuery(projectsDtoIn);

        const projects = await this.projectsRepository.findMany(
            filter,
            sortObject,
            pageInfo,
        );

        const dtoOut = {
            data: projects,
        };

        return dtoOut;
    }

    async findOneById(dtoIn: FindOneProjectDtoIn): Promise<ProjectDtoOut> {
        const filter = this.queryBuilder.buildFindOneQuery(dtoIn);

        const projects = await this.projectsRepository.findOne(filter);

        const project = projects[0];

        if (!project?.id) {
            throw new NotFoundException({
                message: 'Project not found.',
                cause: dtoIn,
            });
        }

        const dtoOut = {
            data: project,
        };

        return dtoOut;
    }

    async createOne(dtoIn: CreateProjectDtoIn): Promise<ProjectDtoOut> {
        const createData = {} as ProjectInsert;

        createData.title = dtoIn.title;
        createData.description = dtoIn.description;
        createData.state = dtoIn.state || PROJECT_CREATED_STATE;

        const projects = await this.projectsRepository.createOne(createData);

        const project = projects[0];

        if (!project?.id) {
            throw new InternalServerErrorException();
        }

        const dtoOut = {
            data: project,
        };

        return dtoOut;
    }

    async updateOneById(dtoIn: UpdateProjectDtoIn): Promise<ProjectDtoOut> {
        const { data: project } = await this.findOneById({ id: dtoIn.id });

        if (project.version !== dtoIn.version) {
            throw new BadRequestException({
                message: 'Incorrect version of document.',
                cause: { version: dtoIn.version },
            });
        }

        const filter = this.queryBuilder.buildUpdateOneQuery({ id: dtoIn.id });

        const projects = await this.projectsRepository.updateOne(
            filter,
            dtoIn.updateData,
        );

        const updatedProject = projects[0];

        if (!updatedProject?.id) {
            throw new InternalServerErrorException();
        }

        const dtoOut = {
            data: updatedProject,
        };

        return dtoOut;
    }

    async deleteOneById(dtoIn: DeleteProjectDtoIn) {
        const { data: project } = await this.findOneById({ id: dtoIn.id });

        if (project.version !== dtoIn.version) {
            throw new BadRequestException({
                message: 'Incorrect version of document.',
                cause: { version: dtoIn.version },
            });
        }

        const filter = this.queryBuilder.buildDeleteOneQuery({
            id: dtoIn.id,
        });

        const projects = await this.projectsRepository.deleteOne(filter);

        const deletedProject = projects[0];

        if (!deletedProject?.id) {
            throw new InternalServerErrorException();
        }

        const dtoOut = {
            data: deletedProject,
        };

        return dtoOut;
    }
}
