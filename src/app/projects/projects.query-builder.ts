import { Injectable } from '@nestjs/common';
import { and, eq, ilike, inArray, or, SQLWrapper } from 'drizzle-orm';

import * as schema from 'src/database/database.schema';

import { PROJECTS_ACTIVE_STATES } from './projects.constants';
import { Project } from './projects.model';

@Injectable()
export class ProjectsQueryBuilder {
    buildFindManyQuery(dtoIn: Partial<Project>): (SQLWrapper | undefined)[] {
        const orValues: (SQLWrapper | undefined)[] = [];
        const andValues: (SQLWrapper | undefined)[] = [];
        const whereValues: (SQLWrapper | undefined)[] = [];

        if (dtoIn.title) {
            const orValue = ilike(schema.projects.title, `%${dtoIn.title}%`);
            orValues.push(orValue);
        }

        if (dtoIn.description) {
            const orValue = ilike(
                schema.projects.description,
                `%${dtoIn.description}%`,
            );
            orValues.push(orValue);
        }

        if (dtoIn.id) {
            const andValue = eq(schema.projects.id, dtoIn.id);
            andValues.push(andValue);
        }

        if (dtoIn.state) {
            const andValue = eq(schema.projects.state, dtoIn.state);
            andValues.push(andValue);
        } else {
            const andValue = inArray(
                schema.projects.state,
                PROJECTS_ACTIVE_STATES as any,
            );
            andValues.push(andValue);
        }

        if (orValues.length > 0) {
            whereValues.push(or(...orValues));
        }

        if (andValues.length > 0) {
            whereValues.push(and(...andValues));
        }

        return whereValues;
    }

    buildFindOneQuery(dtoIn: Partial<Project>): (SQLWrapper | undefined)[] {
        const dtoInKeys = Object.keys(dtoIn) as (keyof Partial<Project>)[];
        const whereValues: (SQLWrapper | undefined)[] = [];

        dtoInKeys.forEach(function (key) {
            const column = schema.projects[key];
            const value = dtoIn[key] as Project[keyof Project];
            const whereValue = eq(column, value);
            whereValues.push(whereValue);
        });

        return whereValues;
    }

    buildUpdateOneQuery(dtoIn: Partial<Project>): SQLWrapper[] {
        const dtoInKeys = Object.keys(dtoIn) as (keyof Partial<Project>)[];
        const whereValues: SQLWrapper[] = [];

        if (dtoInKeys.length === 0) {
            throw new Error('dtoIn should have at least one key-value pair.', {
                cause: dtoIn,
            });
        }

        dtoInKeys.forEach(function (key) {
            const column = schema.projects[key];
            const value = dtoIn[key] as Project[keyof Project];
            const whereValue = eq(column, value);
            whereValues.push(whereValue);
        });

        return whereValues;
    }

    buildDeleteOneQuery(dtoIn: Partial<Project>): SQLWrapper[] {
        const dtoInKeys = Object.keys(dtoIn) as (keyof Partial<Project>)[];
        const whereValues: SQLWrapper[] = [];

        if (dtoInKeys.length === 0) {
            throw new Error('dtoIn should have at least one key-value pair.', {
                cause: dtoIn,
            });
        }

        dtoInKeys.forEach(function (key) {
            const column = schema.projects[key];
            const value = dtoIn[key] as Project[keyof Project];
            const whereValue = eq(column, value);
            whereValues.push(whereValue);
        });

        return whereValues;
    }
}
