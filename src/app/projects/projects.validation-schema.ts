import Joi from 'joi';

import { SORT_ORDER } from 'src/constants';

import {
    CreateProjectDtoIn,
    DeleteProjectDtoIn,
    FindManyProjectDtoIn,
    FindOneProjectDtoIn,
    UpdateProjectData,
    UpdateProjectDtoIn,
} from './projects.model';
import {
    PROJECT_DRAFT_STATE,
    PROJECTS_SORTABLE_FIELDS,
    PROJECTS_STATES,
} from './projects.constants';

export const findManyProjectsSchema = Joi.object<FindManyProjectDtoIn>({
    search: Joi.string().trim().min(1).max(255).optional(),
    state: Joi.string()
        .trim()
        .valid(...PROJECTS_STATES)
        .optional(),
    sortBy: Joi.string()
        .trim()
        .valid(...PROJECTS_SORTABLE_FIELDS)
        .optional(),
    sortOrder: Joi.string()
        .trim()
        .valid(...SORT_ORDER)
        .optional(),
    page: Joi.number().integer().positive().optional(),
    pageSize: Joi.number().integer().positive().optional(),
}).optional();

export const findOneByIdProjectSchema = Joi.object<FindOneProjectDtoIn, true>({
    id: Joi.number().integer().positive().optional(),
}).required();

export const createProjectSchema = Joi.object<CreateProjectDtoIn, true>({
    title: Joi.string().trim().min(1).max(255).required(),
    description: Joi.string().trim().min(1).max(3000).required(),
    state: Joi.string().trim().valid(PROJECT_DRAFT_STATE).optional(),
}).required();

export const updateOneProjectSchema = Joi.object<
    Omit<UpdateProjectDtoIn, 'id'>,
    true
>({
    version: Joi.number().integer().positive().required(),
    updateData: Joi.object<UpdateProjectData>({
        title: Joi.string().trim().min(1).max(255).optional(),
        description: Joi.string().trim().min(1).max(3000).optional(),
        state: Joi.string()
            .trim()
            .valid(...PROJECTS_STATES)
            .optional(),
    })
        .min(1)
        .required(),
}).required();

export const deleteOneProjectSchema = Joi.object<
    Omit<DeleteProjectDtoIn, 'id'>,
    true
>({
    version: Joi.number().integer().positive().required(),
}).required();
