import { Module } from '@nestjs/common';

import { ProjectsController } from './projects.controller';
import { ProjectsQueryBuilder } from './projects.query-builder';
import { ProjectsRepository } from './projects.repository';
import { ProjectsService } from './projects.service';

@Module({
    providers: [ProjectsService, ProjectsRepository, ProjectsQueryBuilder],
    controllers: [ProjectsController],
})
export class ProjectsModule {}
