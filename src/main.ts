import process from 'node:process';

import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { INestApplication, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import helmet from 'helmet';

import { AppModule } from './app/app.module';
import { getPortFromEnv } from './utils';

function onListenerStart(logger: Logger): () => void {
    return function handleListenerStart(): void {
        logger.log('Http listener started.');
    };
}

function setupGracefulShutdown(listener: NodeJS.SignalsListener): void {
    process.on('SIGINT', listener);
    process.on('SIGTERM', listener);
}

function onShutdown(app: INestApplication, logger: Logger) {
    return async function handleShutdown() {
        logger.log('Http listener shutdown...');
        try {
            await app.close();
            logger.log('Http listener stopped.');
        } catch (error) {
            logger.error(error);
        }
    };
}

async function bootstrap() {
    const logger = new Logger('AppBootstrap');
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    const configService = app.get<ConfigService>(ConfigService);
    const port = getPortFromEnv(configService);

    app.set('trust proxy', true);
    app.disable('x-powered-by');

    app.setGlobalPrefix('api');
    app.enableCors();
    app.use(helmet());

    setupGracefulShutdown(onShutdown(app, logger));

    await app.listen(port, onListenerStart(logger));
}
bootstrap();
