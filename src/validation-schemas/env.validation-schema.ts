import Joi from 'joi';

import { ENV_VARS } from 'src/types';
import { ENV_MODES } from 'src/constants';

export const envSchema = Joi.object<ENV_VARS, true>({
    NODE_ENV: Joi.string()
        .trim()
        .valid(...ENV_MODES)
        .required(),
    PORT: Joi.number().integer().positive().required(),
    DATABASE_URL: Joi.string().trim().uri().required(),
}).required();
