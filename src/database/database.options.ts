import { ConfigurableModuleAsyncOptions } from '@nestjs/common';
import { PostgresClientParams } from './postgres/postgres-client';
import { ConfigService } from '@nestjs/config';

import { getValueFromEnv } from 'src/utils';

export const databaseOptions = {
    inject: [ConfigService],
    useFactory: function (configService: ConfigService) {
        const url = getValueFromEnv('DATABASE_URL', configService);

        return { url };
    },
} satisfies ConfigurableModuleAsyncOptions<PostgresClientParams, 'create'> &
    Partial<Record<string, any>>;
