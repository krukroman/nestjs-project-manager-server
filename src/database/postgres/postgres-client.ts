import postgres, { Options, Sql, PostgresType } from 'postgres';

export type PostgresClientParams = {
    url: string;
    options?: Options<Record<string, PostgresType>>;
};

export function postgresClientFactory(params: PostgresClientParams): Sql {
    const { url, options } = params;
    const client = postgres(url, options);
    return client;
}
