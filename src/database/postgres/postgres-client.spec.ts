import { postgresClientFactory } from './postgres-client';

describe('postgresClientFactory', function () {
    it('should return a defined result', function () {
        const url = 'postgresql://host:4321/dbName';
        const options = {};
        const clientWithoutOptions = postgresClientFactory({ url });
        const clientWithOptions = postgresClientFactory({ url, options });

        expect(clientWithoutOptions).toHaveProperty('end');
        expect(clientWithOptions).toHaveProperty('end');
    });
});
