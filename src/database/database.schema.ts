import { relations, sql } from 'drizzle-orm';
import {
    pgTable,
    serial,
    pgEnum,
    varchar,
    timestamp,
    integer,
    index,
} from 'drizzle-orm/pg-core';

import {
    PROJECT_CREATED_STATE,
    PROJECTS_STATES,
} from 'src/app/projects/projects.constants';
import {
    TASK_CREATED_STATE,
    TASKS_STATES,
    TASKS_PRIORITIES,
} from 'src/app/tasks/tasks.constants';

function onUpdatedAtUpdate() {
    return new Date();
}

function onVersionUpdate() {
    return sql<never>`version + 1`;
}

export const projectsStatesEnum = pgEnum('projects_states', PROJECTS_STATES);
export const tasksStatesEnum = pgEnum('tasks_states', TASKS_STATES);
export const tasksPriorityEnum = pgEnum('tasks_priorities', TASKS_PRIORITIES);

export const projects = pgTable(
    'projects',
    {
        id: serial('id').primaryKey(),
        title: varchar('title', {
            length: 255,
        }).notNull(),
        description: varchar('description', {
            length: 3000,
        }).notNull(),
        state: projectsStatesEnum('state')
            .default(PROJECT_CREATED_STATE)
            .notNull(),

        created_at: timestamp('created_at', {
            mode: 'date',
            precision: 3,
            withTimezone: true,
        })
            .defaultNow()
            .notNull(),
        updated_at: timestamp('updated_at', {
            mode: 'date',
            precision: 3,
            withTimezone: true,
        })
            .defaultNow()
            .$onUpdateFn(onUpdatedAtUpdate)
            .notNull(),
        version: integer('version')
            .default(1)
            .$onUpdateFn(onVersionUpdate)
            .notNull(),
    },
    function (projects) {
        return {
            title_desc_idx: index('projects_title_description_idx').on(
                projects.title,
                projects.description,
            ),
            state_idx: index('projects_state_idx').on(projects.state),
        };
    },
);

export const tasks = pgTable(
    'tasks',
    {
        id: serial('id').primaryKey(),
        title: varchar('title', {
            length: 255,
        }).notNull(),
        description: varchar('description', {
            length: 3000,
        }).notNull(),
        state: tasksStatesEnum('state').default(TASK_CREATED_STATE).notNull(),
        priority: tasksPriorityEnum('priority').notNull(),
        project_id: integer('project_id')
            .references(
                function () {
                    return projects.id;
                },
                {
                    onDelete: 'cascade',
                    onUpdate: 'no action',
                },
            )
            .notNull(),

        created_at: timestamp('created_at', {
            mode: 'date',
            precision: 3,
            withTimezone: true,
        })
            .defaultNow()
            .notNull(),
        updated_at: timestamp('updated_at', {
            mode: 'date',
            precision: 3,
            withTimezone: true,
        })
            .defaultNow()
            .$onUpdateFn(onUpdatedAtUpdate)
            .notNull(),
        version: integer('version')
            .default(1)
            .$onUpdateFn(onVersionUpdate)
            .notNull(),
    },
    function (tasks) {
        return {
            title_desc_idx: index('tasks_title_description_idx').on(
                tasks.title,
                tasks.description,
            ),
            state_idx: index('tasks_state_idx').on(tasks.state),
            priority_idx: index('tasks_priority_idx').on(tasks.priority),
            project_id_idx: index('tasks_project_id_idx').on(tasks.project_id),
        };
    },
);

export const projectRelations = relations(projects, function ({ many }) {
    return {
        tasks: many(tasks),
    };
});

export const tasksRelations = relations(tasks, function ({ one }) {
    return {
        project: one(projects, {
            fields: [tasks.project_id],
            references: [projects.id],
        }),
    };
});
