import process from 'node:process';

import { Test, TestingModule } from '@nestjs/testing';
import { DrizzleService } from './drizzle.service';
import { DatabaseModule } from '../database.module';
import { databaseOptions } from '../database.options';
import { ConfigModule } from '@nestjs/config';

describe('DrizzleService', function () {
    const originalEnv = process.env;
    let drizzleService: DrizzleService;

    beforeEach(async function () {
        process.env.DATABASE_URL = 'postgresql://host:4321/dbName';
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot({
                    isGlobal: true,
                    ignoreEnvFile: true,
                }),
                DatabaseModule.forRootAsync(databaseOptions),
            ],
        }).compile();

        drizzleService = module.get<DrizzleService>(DrizzleService);
    });

    afterEach(function () {
        process.env = originalEnv;
    });

    it('should have db property.', function () {
        expect(drizzleService.db).toHaveProperty('select');
        expect(drizzleService.db).toHaveProperty('insert');
        expect(drizzleService.db).toHaveProperty('update');
        expect(drizzleService.db).toHaveProperty('delete');
        expect(drizzleService.db).toHaveProperty('query');
        expect(drizzleService.db).toHaveProperty('execute');
    });
});
