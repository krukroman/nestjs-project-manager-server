import { env } from 'node:process';
import { resolve } from 'node:path';
import { info } from 'node:console';

import { drizzle } from 'drizzle-orm/postgres-js';
import { migrate } from 'drizzle-orm/postgres-js/migrator';
import postgres from 'postgres';

const migrationsFolder = resolve(__dirname, '../../..', 'drizzle');

const client = postgres(env.DATABASE_URL!, { max: 1 });

const drizzleClient = drizzle(client);

async function runMigration(): Promise<void> {
    const timeout = setTimeout(async function () {
        try {
            info('Migration start...');

            await migrate(drizzleClient, {
                migrationsFolder,
            });

            info('Migration finish.');
        } catch (error) {
            throw error;
        } finally {
            await client.end();
            clearTimeout(timeout);
        }
    }, 5000);
}

function onError(error: unknown) {
    throw error;
}

runMigration().catch(onError);
