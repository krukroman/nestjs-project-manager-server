import { Inject, Injectable } from '@nestjs/common';
import { PostgresJsDatabase, drizzle } from 'drizzle-orm/postgres-js';
import { Sql } from 'postgres';

import { DB_CONNECTION_TOKEN } from 'src/constants';

import * as schema from '../database.schema';

@Injectable()
export class DrizzleService {
    db: PostgresJsDatabase<typeof schema>;

    constructor(@Inject(DB_CONNECTION_TOKEN) client: Sql) {
        this.db = drizzle(client, { schema });
    }
}
