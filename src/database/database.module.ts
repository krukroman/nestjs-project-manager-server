import {
    ConfigurableModuleBuilder,
    Global,
    Module,
    Provider,
} from '@nestjs/common';

import { DB_CONNECTION_TOKEN } from 'src/constants';

import {
    postgresClientFactory,
    PostgresClientParams,
} from './postgres/postgres-client';
import { DrizzleService } from './drizzle/drizzle.service';

const {
    MODULE_OPTIONS_TOKEN: POSTGRES_CLIENT_PARAMS,
    ConfigurableModuleClass: ConfigurableDatabaseModule,
} = new ConfigurableModuleBuilder<PostgresClientParams>()
    .setClassMethodName('forRoot')
    .build();

const PostgresClientProvider = {
    provide: DB_CONNECTION_TOKEN,
    useFactory: postgresClientFactory,
    inject: [POSTGRES_CLIENT_PARAMS],
} satisfies Provider;

@Global()
@Module({
    providers: [DrizzleService, PostgresClientProvider],
    exports: [DrizzleService],
})
export class DatabaseModule extends ConfigurableDatabaseModule {}
