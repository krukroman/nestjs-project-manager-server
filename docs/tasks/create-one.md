[Back](./tasks.md)

### `POST "/api/tasls"`

#### Body

```typescript
type Body = {
    title: string; // max length 255
    description: string; // max length 3000
    priority: 'A' | 'B' | 'C' | 'D' | 'E';
    project_id: number;
};
```

#### Returns

```typescript
const httpStatus = 200;

type DtoOut = {
    data: {
        id: number;
        title: string;
        description: string;
        state: 'created';
        priority: 'A' | 'B' | 'C' | 'D' | 'E';
        project_id: number;
        created_at: string; // date-string
        updated_at: string; // date-string
        version: number;
    };
};
```

#### Errors

```typescript
const httpStatus = 400; // bad request
const httpStatus = 404; // related project not found
const httpStatus = 422; // validation error
const httpStatus = 500; // server error
```
