[Back](./tasks.md.md)

### `GET "/api/tasks/:id"`

#### Params

```typescript
type Params = {
    id: number;
};
```

#### Returns

```typescript
const httpStatus = 200;

type DtoOut = {
    data: {
        id: number;
        title: string;
        description: string;
        state: 'created' | 'completed' | 'archived' | 'in_progress';
        priority: 'A' | 'B' | 'C' | 'D' | 'E';
        project_id: number;
        created_at: string; // date-string
        updated_at: string; // date-string
        version: number;
    };
};
```

#### Errors

```typescript
const httpStatus = 404; // not found error
const httpStatus = 422; // validation error
const httpStatus = 500; // server error
```
