[Back](./tasks.md)

### `GET "/api/tasks"`

#### Query string

```typescript
type FindManyTasksDtoIn = {
    search?: string; // max length 255
    state?: 'created' | 'completed' | 'archived' | 'in_progress'; // if state not provided api will return all projects that have state 'created' or 'in_progress'
    priority?: 'A' | 'B' | 'C' | 'D' | 'E';
    project_id?: number;
    sortBy?: 'title' | 'state' | 'created_at'; // default "created_at"
    sortOrder?: 'asc' | 'desc'; // default "desc"
    page?: number; // default 1
    pageSize?: number; // default 100, max 300
};
```

#### Returns

```typescript
const httpStatus = 200;

type DtoOut = {
    data: {
        id: number;
        title: string;
        description: string;
        state: 'created' | 'completed' | 'archived' | 'in_progress';
        priority: 'A' | 'B' | 'C' | 'D' | 'E';
        project_id: number;
        created_at: string; // date-string
        updated_at: string; // date-string
        version: number;
    }[];
};
```

#### Errors

```typescript
const httpStatus = 422; // validation error
const httpStatus = 500; // server error
```
