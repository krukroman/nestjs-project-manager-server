[Back](../app.md)

### `/tasks`

-   `GET "/"` - [docs](./find-many.md)
-   `POST "/"` - [docs](./create-one.md)
-   `GET "/:id"` - [docs](./find-one.md)
-   `PUT "/:id"` - [docs](./update-one.md)
-   `DELETE "/:id"` - [docs](./delete-one.md)
