[Back](./projects.md)

### `GET "/api/projects"`

#### Query string

```typescript
type FindManyProjectDtoIn = {
    search?: string; // max length 255
    state?: 'draft' | 'created' | 'active' | 'completed' | 'archived'; // if state not provided api will return all projects that have state 'created' or 'active'
    sortBy?: 'title' | 'state' | 'created_at'; // default "created_at"
    sortOrder?: 'asc' | 'desc'; // default "desc"
    page?: number; // default 1
    pageSize?: number; // default 100, max 300
};
```

#### Returns

```typescript
const httpStatus = 200;

type DtoOut = {
    data: {
        id: number;
        title: string;
        description: string;
        state: 'draft' | 'created' | 'active' | 'completed' | 'archived';
        created_at: string; // date-string
        updated_at: string; // date-string
        version: number;
    }[];
};
```

#### Errors

```typescript
const httpStatus = 422; // validation error
const httpStatus = 500; // server error
```
