[Back](../app.md)

### `/api/projects`

-   `GET "/api/projects"` - [docs](./find-many.md)
-   `POST "/api/projects"` - [docs](./create-one.md)
-   `GET "/api/projects/:id"` - [docs](./find-one.md)
-   `PUT "/api/projects/:id"` - [docs](./update-one.md)
-   `DELETE "/api/projects/:id"` - [docs](./delete-one.md)
