[Back](./projects.md)

### `POST "/api/projects"`

#### Body

```typescript
type Body = {
    title: string; // max length 255
    description: string; // max length 3000
    state?: 'draft'; // if state not provided, 'created' will be used
};
```

#### Returns

```typescript
const httpStatus = 200;

type DtoOut = {
    data: {
        id: number;
        title: string;
        description: string;
        state: 'draft' | 'created';
        created_at: string; // date-string
        updated_at: string; // date-string
        version: number;
    };
};
```

#### Errors

```typescript
const httpStatus = 400; // bad request
const httpStatus = 422; // validation error
const httpStatus = 500; // server error
```
