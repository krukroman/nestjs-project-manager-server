[Back](../README.md)

### App docs

#### Main route

`http://<hostname>:<port>/api`

#### Routes

-   `/projects` - [docs](./projects/projects.md)
-   `/tasks` - [docs](./tasks/tasks.md)
