DO $$ BEGIN
 CREATE TYPE "public"."projects_states" AS ENUM('draft', 'created', 'active', 'completed', 'archived');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 CREATE TYPE "public"."tasks_states" AS ENUM('created', 'in_progress', 'completed', 'archived');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "projects" (
	"id" serial PRIMARY KEY NOT NULL,
	"title" varchar(255) NOT NULL,
	"description" varchar(3000) NOT NULL,
	"state" "projects_states" DEFAULT 'created' NOT NULL,
	"created_at" timestamp (3) with time zone DEFAULT now() NOT NULL,
	"updated_at" timestamp (3) with time zone DEFAULT now() NOT NULL,
	"version" integer DEFAULT 1 NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "tasks" (
	"id" serial PRIMARY KEY NOT NULL,
	"title" varchar(255) NOT NULL,
	"description" varchar(3000) NOT NULL,
	"state" "tasks_states" DEFAULT 'created' NOT NULL,
	"project_id" integer NOT NULL,
	"created_at" timestamp (3) with time zone DEFAULT now() NOT NULL,
	"updated_at" timestamp (3) with time zone DEFAULT now() NOT NULL,
	"version" integer DEFAULT 1 NOT NULL
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "tasks" ADD CONSTRAINT "tasks_project_id_projects_id_fk" FOREIGN KEY ("project_id") REFERENCES "public"."projects"("id") ON DELETE cascade ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "projects_title_description_idx" ON "projects" USING btree ("title","description");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "projects_state_idx" ON "projects" USING btree ("state");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "tasks_title_description_idx" ON "tasks" USING btree ("title","description");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "tasks_state_idx" ON "tasks" USING btree ("state");--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "tasks_project_id_idx" ON "tasks" USING btree ("project_id");