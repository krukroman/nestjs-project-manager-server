DO $$ BEGIN
 CREATE TYPE "public"."tasks_priorities" AS ENUM('A', 'B', 'C', 'D', 'E');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
ALTER TABLE "tasks" ADD COLUMN "priority" "tasks_priorities" NOT NULL;--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "tasks_priority_idx" ON "tasks" USING btree ("priority");