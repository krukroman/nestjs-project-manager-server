#### nestjs-project-manager-server

---

A training application about managing projects.

---

#### Stack:

-   [Typescript](https://www.typescriptlang.org/) - Language
-   [NestJS](https://nestjs.com/) - Framework
-   [PostgreSQL](https://www.postgresql.org/) - Database
-   [Drizzle-Orm](https://orm.drizzle.team/) - Orm
-   [Drizzle-Kit](https://orm.drizzle.team/kit-docs/overview) - Cli for database
    migrations
-   [Postgres.js](https://github.com/porsager/postgres) - PostgreSQL client
-   [Joi](https://joi.dev/) - Validation library
-   [Jest](https://jestjs.io/) - Testing library

#### How to start

1. Pull or copy this repo onto your local machine;

2. Fill [.env.docker.example](.env.docker.example) file and rename to
   `.env.docker`;
3. Run command `npm run docker:start`;

#### How to make a request

1. Read [app](./docs/app.md) docs;

###### Optional:

2. Import [`insomnia`](./insomnia/Insomnia_2024-07-05.json) file to your
   [Insomnia app](https://insomnia.rest/download) and play with prepared
   requests;
